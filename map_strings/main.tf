terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "test" {
    type = map(string)
    default = {"test1" = "value1", "test2" = "value2"}
}

resource "aws_iam_role" "foobar" {
    for_each    = var.test
    name        = each.key

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "${each.value}"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}