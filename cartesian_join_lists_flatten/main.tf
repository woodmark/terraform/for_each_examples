terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "list1" {
    type = list(string)
    default = ["test1", "test2"]
}

variable "list2" {
    type = list(string)
    default = ["test3", "test4"]
}

locals {
  # Loop over both lists and flatten the result
  cartesian_join = flatten([
    for value1 in var.list1 : [
      for value2 in var.list2 : {
        value1 = value1
        value2 = value2
      }
    ]
  ])
}

resource "aws_iam_role" "foobar" {
    for_each    = {
      for object in local.cartesian_join: "${object.value1}-${object.value2}" => object # Note, name need to be unique (or whatever you use as the key)
    }

    name        = each.key

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "${each.value.value2}"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}