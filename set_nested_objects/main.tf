terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "test" {
    type = set(object({
      name = string
      config = object({
        Action  = string
        Effect  = string
        Sid     = string
        Service = string
      })
    }))
    default = [
      {
        name = "test1",
        config = {
          Action = "sts:AssumeRole"
          Effect = "Allow"
          Sid    = "value1"
          Service = "ec2.amazonaws.com"
        }
      },
      {
        name = "test2",
        config = {
          Action = "sts:AssumeRole"
          Effect = "Allow"
          Sid    = "value2"
          Service = "ec2.amazonaws.com"
        }
      }
    ]
}

resource "aws_iam_role" "foobar" {
    for_each    = {
      for object in var.test: object.name => object.config # Note, name need to be unique (or whatever you use as the key)
    }

    name        = each.key

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "${each.value.Action}"
        Effect = "${each.value.Effect}"
        Sid    = "${each.value.Sid}"
        Principal = {
          Service = "${each.value.Service}"
        }
      },
    ]
  })
}