terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "test" {
    type = list(object({
      name = string
      sid  = string
    }))
    default = [
      {
        name = "test1",
        sid  = "value1"
      },
      {
        name = "test2",
        sid  = "value2"
      }
    ]
}

resource "aws_iam_role" "foobar" {
    for_each    = {
      for object in var.test: object.name => object.sid # Note, name need to be unique (or whatever you use as the key)
    }

    name        = each.key

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "${each.value}"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}