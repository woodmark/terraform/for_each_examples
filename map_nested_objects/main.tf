terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "test" {
    type = map(object({
      description = string
      config = object({
        Action  = string
        Effect  = string
        Sid     = string
        Service = string
      })
    }))
    default = {
      "test1" = {
        description = "description1",
        config = {
          Action = "sts:AssumeRole"
          Effect = "Allow"
          Sid    = "value1"
          Service = "ec2.amazonaws.com"
        }
      },
      "test2" = {
        description = "description2",
        config = {
          Action = "sts:AssumeRole"
          Effect = "Allow"
          Sid    = "value2"
          Service = "ec2.amazonaws.com"
        }
      }
    }
}

resource "aws_iam_role" "foobar" {
    for_each    = var.test

    name        = each.key
    description = each.value.description

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "${each.value.config.Action}"
        Effect = "${each.value.config.Effect}"
        Sid    = "${each.value.config.Sid}"
        Principal = {
          Service = "${each.value.config.Service}"
        }
      },
    ]
  })
}